# [HowerPause](#howerpause) 

Demo application for saving profiles and will process metering data coming from customer connections. 

### [Prerequisites](#preresquisites)

* Git
* JDK 8 or later
* Maven 3.0 or later

#### [Clone](#clone)

To get started you can simply clone this repository using git:
```
	git clone https://bitbucket.org/tin0097/powercons.git
    cd powetcons
```
#### [Configuration](#configuration)

Database configuration is set in the ```src/main/resources/application.properties```. Please use following configuration to enable H2 in memory database.

```properties
    # H2
    spring.h2.console.enabled=true
    spring.h2.console.path=/h2
    # Datasource
    spring.datasource.url=jdbc:h2:mem:powercons;DB_CLOSE_ON_EXIT=FALSE
    spring.datasource.username=sa
    spring.datasource.password=
    spring.datasource.driver-class-name=org.h2.Driver
```
#### [Start the application](###start-the-application)

To start the applicaton, please execute the following command
``` 
	mvn spring-boot:run
```
#### [Run the tests](#run-the-tests)

To execute the tests, just execute 

```
	mvn test
```

#### [Author](#author)
For more information please contact [Konstantin Sivakov](konstantin.sivakov@gmail.com) or find me on [LinkedIn](https://www.linkedin.com/in/konstantinsivakov/) and [Github](https://github.com/tino097)