/**
 * 
 */
package com.sivakov.howerpause.controllers.requests;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.sivakov.howerpause.models.Profile;

/**
 * 
 * The {@link ProfileRequest} is the model class to request profile
 *
 * @author Tino097
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ProfileRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@JsonProperty("profiles")
	private List<Profile> profiles;

	public List<Profile> getProfiles() {
		return profiles;
	}

	public void setProfiles(List<Profile> profiles) {
		this.profiles = profiles;
	}

	public ProfileRequest() {
	}

	public ProfileRequest(List<Profile> profiles) {
		this.profiles = profiles;
	}

	@Override
	public String toString() {
		return "ProfileRequest [profiles=" + profiles + "]";
	}

}
