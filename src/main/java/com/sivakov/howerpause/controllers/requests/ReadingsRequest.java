package com.sivakov.howerpause.controllers.requests;

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sivakov.howerpause.models.Readings;

/**
 * 
 * @author Tino097
 *
 */
public class ReadingsRequest implements Serializable {

	/**
	 * 
	 */
	@JsonProperty("readings")
	private static final long serialVersionUID = 1L;

	private List<Readings> readings;

	/**
	 * @return the readings
	 */
	public List<Readings> getReadings() {
		return readings;
	}

	/**
	 * @param readings
	 *            the readings to set
	 */
	public void setReadings(List<Readings> readings) {
		this.readings = readings;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ReadingsRequest [readings=" + readings + "]";
	}

}
