/**
 * 
 */
package com.sivakov.howerpause.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sivakov.howerpause.controllers.requests.ReadingsRequest;
import com.sivakov.howerpause.controllers.responses.GenericResponse;
import com.sivakov.howerpause.models.Readings;
import com.sivakov.howerpause.services.ReadingsService;

/**
 * @author Tino097
 *
 */
@RestController("readingsController")
public class ReadingsController extends BaseController {

	@Autowired
	private ReadingsService readingsService;

	public ReadingsController(ReadingsService readingsService) {
		this.readingsService = readingsService;
	}

	@RequestMapping(value = "/readings", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	public GenericResponse<?> addReadings(@RequestBody ReadingsRequest readingsRequest) {
		LOG.info("Received request message: " + readingsRequest);
		List<Readings> errorReadings = readingsService.save(readingsRequest.getReadings());
//		for (Readings reading : readingsRequest.getReadings()) {
//			Long result = readingsService.save(reading);
//
//		}
		return new GenericResponse<List<Readings>>();
	}
}
