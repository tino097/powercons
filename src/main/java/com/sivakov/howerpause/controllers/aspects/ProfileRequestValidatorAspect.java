/**
 * 
 */
package com.sivakov.howerpause.controllers.aspects;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.sivakov.howerpause.controllers.exceptions.BadRequestException;
import com.sivakov.howerpause.controllers.exceptions.ProfileErrorException;
import com.sivakov.howerpause.controllers.requests.ProfileRequest;
import com.sivakov.howerpause.models.Profile;
import com.sivakov.howerpause.models.ProfileType;

/**
 * 
 * The {@ProfileRequestValidatorAspect} is used to intercept request to poincuts
 * and perform validation
 * 
 * @author Tino097
 *
 */
@Aspect
@Component
@Order(2)
public class ProfileRequestValidatorAspect {

	private static final Logger LOG = Logger.getLogger(ProfileRequestValidatorAspect.class);

	@Autowired
	private HttpServletRequest context; // if we need to validate headers

	/*
	 * Simple validator using Aspects and @Before advice return Invalid request if
	 * data is missing
	 * 
	 * I find this way of validating good as most of the errors could be detected
	 * before actual processing of the data down the layers
	 * 
	 */
	@Before(value = "(execution(* com.sivakov.howerpause.controllers.*.addProfiles(..)) && args(request))")
	public void validateProfileRequest(ProfileRequest request) {
		LOG.info("Validating profile request " + request);
		validateHeaders();
		if (request == null) {
			LOG.error("Invalid request");
			throw new BadRequestException("Invalid request");
		}

		if (request.getProfiles() == null) {
			LOG.error("No Data Found");
			throw new BadRequestException("No Data Found");
		}
		checkProfiles(request);
		checkFractionsSum(request);
	}

	/*
	 * 
	 * Simple check if there are profile fractions for all months, assume there
	 * won't be duplicates
	 * 
	 * @param request
	 * 
	 */
	private void checkProfiles(ProfileRequest request) {
		if (request.getProfiles().size() % 12 != 0) {
			LOG.error("Missing some data in profiles");
			throw new ProfileErrorException("Missing some data in profiles");
		}

	}

	/*
	 * One implementation of api security to get the authorization header and parse
	 * the username and password or api key spring security Base64.decode()
	 * 
	 */
	private void validateHeaders() {

		String authorization = this.context.getHeader("authorization");
		LOG.info("Authorization passed");
	}

	/*
	 * For a given Profile (A or B in the example) the sum of all fractions should
	 * be 1. It represents the ratio of consumption that is expected each month. If
	 * input data is not fulfilling this condition then an error should be raised
	 * 
	 * @param ProfileRequest
	 */
	private void checkFractionsSum(ProfileRequest request) {
		// Assuming that we will have configuration options containing all profiles
		// this is one solution to validate fractions value if it is over the limit
		// (not the greatest though plus weird warning if streams are used)
		for (ProfileType profileType : ProfileType.values()) {
			double sum = 0;
			for (Profile profile : request.getProfiles()) {

				if (profileType.toString().equals(profile.getProfileType())) {
					sum += profile.getFraction();
				}
				if (sum > 1)
					throw new BadRequestException("Fractions for profile " + profileType + " are over limit!");

			}

		}

	}
}
