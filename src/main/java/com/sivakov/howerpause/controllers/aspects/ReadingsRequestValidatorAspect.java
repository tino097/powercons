/**
 * 
 */
package com.sivakov.howerpause.controllers.aspects;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.sivakov.howerpause.controllers.exceptions.BadRequestException;
import com.sivakov.howerpause.controllers.requests.ReadingsRequest;

/**
 * The {@ReadingsRequestValidatorAspect} is used to intercept request to
 * poincuts and perform validation
 * 
 * @author Tino097
 *
 */
@Aspect
@Component
@Order(2)
public class ReadingsRequestValidatorAspect {

	private static final Logger LOG = Logger.getLogger(ReadingsRequestValidatorAspect.class);

	@Autowired
	private HttpServletRequest context;
	/*
	 * Simple validator using Aspects and @Before advice return Invalid request if
	 * data is missing
	 * 
	 * I find this way of validating good as most of the errors could be detected
	 * before actual processing of the data down the layers
	 * 
	 */

	@Before(value = "(execution(* com.sivakov.howerpause.controllers.*.addReadings(..)) && args(request))")
	public void validateReadingsRequest(ReadingsRequest request) {
		LOG.info("Validating readings request " + request);
		if (request == null) {
			LOG.error("Invalid request");
			throw new BadRequestException("Invalid request");
		}

	}
}
