/**
 * 
 */
package com.sivakov.howerpause.controllers.utils;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.sivakov.howerpause.controllers.exceptions.ReadingsErrorException;
import com.sivakov.howerpause.models.Profile;
import com.sivakov.howerpause.models.ProfileType;
import com.sivakov.howerpause.models.Readings;
import com.sivakov.howerpause.services.ProfileService;

/**
 * The {@link Validator} is helper/utils class where some of the validations
 * could performed
 * 
 * @author Tino097
 *
 */
@Component
@Scope("prototype")
public class Validator {

	private static final Logger LOG = Logger.getLogger(Validator.class);

	@Autowired
	ProfileService profileService;

	/**
	 * //For a given connection a meter reading for a month should not be lower than
	 * the previous one.
	 * 
	 * @param list
	 * @return
	 */
	public void validateMonthlyReadings(List<Readings> list) {
		// First the list with readings is transformed to Map<ConnectionId, Readings>
		// for better iteration as the rows are in random order
		Map<String, List<Readings>> mapList = list.stream().collect(Collectors.groupingBy(p -> p.getConnectionId()));

		for (Map.Entry<String, List<Readings>> entry : mapList.entrySet()) {
			String connectionId = entry.getKey();
			List<Readings> readingsList = entry.getValue();
			readingsList.sort(new Comparator<Readings>() {
				public int compare(Readings r1, Readings r2) {
					return (r1.getMonth().compareTo(r2.getMonth()));
				}
			});
			for (Iterator<Readings> iterator = readingsList.iterator(); iterator.hasNext();) {
				Readings readings = (Readings) iterator.next();
				List<Profile> profiles = profileService.findByProfileType(readings.getProfileType().name());
				if (profiles == null) {
					throw new ReadingsErrorException("No Profile found!");
				}
				// for (Profile profile : profiles) {
				// if (profile.getMonth() == readings.getMonth()) {
				//
				// Missed the design for consumption
				// }
				// }
				while (iterator.hasNext()) {
					Readings next = (Readings) iterator.next();
					if (readings.getMeterReading() > next.getMeterReading()) {
						LOG.error("Error in month readings for connection ID " + connectionId);
						throw new ReadingsErrorException("Some of the monthly values are incorrect");
					}
					readings = next;
				}

			}
		}

	}

}
