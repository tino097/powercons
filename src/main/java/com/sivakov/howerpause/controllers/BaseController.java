/**
 * 
 */
package com.sivakov.howerpause.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 
 * The {@link BaseController} is abstract class
 * 
 * @author Tino097 
 */
@RestController
@RequestMapping(value = "/api")
public abstract class BaseController {

	protected Logger LOG = LoggerFactory.getLogger(this.getClass());

}
