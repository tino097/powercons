package com.sivakov.howerpause.controllers.responses;

/**
 * @author Tino097
 *
 *   The {@link ErrorResponse} is model class to error responses
 *   
 */
public class ErrorResponse {

	private String status;
	private String message;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}
