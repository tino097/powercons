/**
 * 
 */
package com.sivakov.howerpause.controllers.responses;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * The {@link GenericResponse} is example for generics usage
 * 
 * @author Tino097
 *
 */
@JsonInclude(Include.NON_NULL)
public class GenericResponse<T> implements Serializable {

	private static final long serialVersionUID = 1L;
	// Returning statusCode as String for better handling if other system is using
	// this api
	private String statusCode;
	private String message;
	private T data;

	public GenericResponse(String message, String statusCode, T data) {

		this.statusCode = statusCode;
		this.message = message;
		this.data = data;
	}

	public GenericResponse(String message, String statusCode) {
		this.statusCode = statusCode;
		this.message = message;
	}

	public GenericResponse() {

	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	/**
	 * @return the statusCode
	 */
	public String getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode
	 *            the statusCode to set
	 */
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
