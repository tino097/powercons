/**
 * 
 */
package com.sivakov.howerpause.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.sivakov.howerpause.controllers.responses.GenericResponse;
import com.sivakov.howerpause.models.Consumption;
import com.sivakov.howerpause.services.ConsumptionService;

/**
 * 
 * 
 * @author tino097
 *
 */
@RestController("consumptionController")
public class ConsumptionController extends BaseController {

	@Autowired
	ConsumptionService consumptionService;

	@RequestMapping(value = "/consumptions", params = "connectionId", method = RequestMethod.GET)
	public GenericResponse<?> getConsumptionForConnectionId(@RequestParam("connectionId") String connectionId) {

		Consumption consumption = consumptionService.getConsumptionForConnectionId(connectionId);

		return new GenericResponse<Consumption>("Success", "200", consumption);
	}

	@RequestMapping(value = "/consumptions", params = "month", method = RequestMethod.GET)
	public GenericResponse<?> getConsumptionForMonth(@RequestParam("month") String month) {

		Consumption consumption = consumptionService.getConsumptionForMonth(month);

		return new GenericResponse<Consumption>("Success", "200", consumption);
	}
}
