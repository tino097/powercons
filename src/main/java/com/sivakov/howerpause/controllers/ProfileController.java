/**
 * 
 */
package com.sivakov.howerpause.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sivakov.howerpause.controllers.exceptions.BadRequestException;
import com.sivakov.howerpause.controllers.exceptions.ProfileErrorException;
import com.sivakov.howerpause.controllers.requests.ProfileRequest;
import com.sivakov.howerpause.controllers.responses.GenericResponse;
import com.sivakov.howerpause.models.Profile;
import com.sivakov.howerpause.services.ProfileService;

/**
 *
 * The {@link ProfileController} handles the rest service requests and returns
 * the data from the database.
 *
 * @author Tino097
 */
@RestController("profilesController")
public class ProfileController extends BaseController {

	@Autowired
	ProfileService profileService;

	public ProfileController(ProfileService profileService) {
		this.profileService = profileService;
	}

	/**
	 * 
	 * @param profileId
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/profiles/{profileId}", method = RequestMethod.GET)
	public GenericResponse<Profile> getProfileById(@PathVariable Long profileId)
			throws BadRequestException, ProfileErrorException {
		Profile profile = null;
		try {
			profile = profileService.getOne(profileId);
		} catch (Exception e) {
			LOG.error("Unable to retreive data" + e.getMessage());
			throw new ProfileErrorException("Unable to retreive data for %s" + profileId);
		}

		return new GenericResponse<Profile>("Success", String.valueOf(HttpStatus.OK), profile);
	}

	/**
	 * 
	 * @param profileRequst
	 * @return list of the profiles
	 * @throws Exception
	 */
	@RequestMapping(value = "/profiles", method = RequestMethod.POST, consumes = {
			MediaType.APPLICATION_JSON_VALUE }, produces = { MediaType.APPLICATION_JSON_VALUE })
	@ResponseStatus(HttpStatus.CREATED)
	public GenericResponse<?> addProfiles(@RequestBody ProfileRequest profileRequst)
			throws BadRequestException, ProfileErrorException {
		LOG.info("Received request message: " + profileRequst);
		if (profileRequst != null) {
			for (Profile p : profileRequst.getProfiles()) {
				try {
					Long result = profileService.save(p);
					if (result == null) {
						LOG.error("Error while saving profile " + p);
						throw new ProfileErrorException("Error while saving profile " + p);
					}
				} catch (Exception e) {
					LOG.error("Unable to save profile " + e.getMessage());
					e.printStackTrace();
				}
			}
		}
		// just to fill with data, could be a list of ID's
		List<Profile> profiles = profileService.getAll();
		return new GenericResponse<List<Profile>>("Successfuly saved profiles", String.valueOf(HttpStatus.CREATED),
				profiles);
	}

	/**
	 * 
	 * @param profileType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/profiles/profile/{profileType}", method = RequestMethod.DELETE, produces = {
			MediaType.APPLICATION_JSON_VALUE })
	public GenericResponse<String> deleteProfilesByType(@PathVariable String profileType)
			throws BadRequestException, ProfileErrorException {
		LOG.info("Deleting profile type " + profileType + " data");
		int result = 0;
		result = profileService.deleteProfilesByType(profileType);
		if (result == 0) {
			throw new ProfileErrorException("Delete failed!");
		}
		return new GenericResponse<String>("Successfuly deleted profile type ".concat(profileType),
				String.valueOf(HttpStatus.NO_CONTENT));
	}

	/**
	 * 
	 * @param profileType
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/profiles/profile/{profileType}", method = RequestMethod.GET)
	public GenericResponse<?> getProfileByType(@PathVariable String profileType)
			throws BadRequestException, ProfileErrorException {
		LOG.info("Retrieve profile type " + profileType + " data");
		List<Profile> profiles = profileService.findByProfileType(profileType);
		if (profiles.isEmpty() || profiles == null) {
			return new GenericResponse<String>("No data for profile for profile type ".concat(profileType),
					String.valueOf(HttpStatus.NO_CONTENT));
		}
		return new GenericResponse<List<Profile>>("Success", String.valueOf(HttpStatus.OK), profiles);
	}
}
