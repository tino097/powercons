/**
 * 
 */
package com.sivakov.howerpause.controllers.exceptions;

/**
 * @author Tino097
 * 
 *         The {@link ProfileErrorException} is thrown when one or more values
 *         set in the request are invalid.
 */
public class ProfileErrorException extends RuntimeException {

	private static final long serialVersionUID = -2779876312423988439L;

	public ProfileErrorException(String message) {
		super(message);
	}

	public ProfileErrorException(String message, Throwable cause) {
		super(message, cause);
	}

}
