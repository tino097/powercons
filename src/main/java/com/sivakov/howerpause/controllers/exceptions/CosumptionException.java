/**
 * 
 */
package com.sivakov.howerpause.controllers.exceptions;

/**
 * 
 * The{@link CosumptionException} is handling exceptions and return adequate
 * messages.
 * 
 * @author Tino097
 */
public class CosumptionException extends RuntimeException {

	private static final long serialVersionUID = -5180338840273411164L;

	public CosumptionException(String message) {
		super(message);
	}

	public CosumptionException(String message, Throwable cause) {
		super(message, cause);
	}

}
