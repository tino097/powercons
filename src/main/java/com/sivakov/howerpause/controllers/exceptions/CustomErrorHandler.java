package com.sivakov.howerpause.controllers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.sivakov.howerpause.controllers.responses.GenericResponse;

/**
 * 
 * The{@link CustomErrorHandler} is handling exceptions and return adequate
 * messages.
 * 
 * @author Tino097
 */
@RestControllerAdvice
public class CustomErrorHandler {

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public GenericResponse<String> handleBadRequest(BadRequestException e) {
		GenericResponse<String> errorResponse = new GenericResponse<String>();
		errorResponse.setMessage(e.getMessage());
		errorResponse.setStatusCode(String.valueOf(HttpStatus.BAD_REQUEST));
		return errorResponse;
	}

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
	public GenericResponse<String> handleExceptions(Exception e) {
		GenericResponse<String> errorResponse = new GenericResponse<String>();
		errorResponse.setMessage(e.getMessage());
		errorResponse.setStatusCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR));
		return errorResponse;
	}

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public GenericResponse<String> handleProfileExceptions(ProfileErrorException e) {
		GenericResponse<String> errorResponse = new GenericResponse<String>();
		errorResponse.setMessage(e.getMessage());
		errorResponse.setStatusCode(String.valueOf(HttpStatus.BAD_REQUEST));
		return errorResponse;
	}

	@ExceptionHandler
	@ResponseStatus(code = HttpStatus.BAD_REQUEST)
	public GenericResponse<String> handleReadingsExceptions(ReadingsErrorException e) {
		GenericResponse<String> errorResponse = new GenericResponse<String>();
		errorResponse.setMessage(e.getMessage());
		errorResponse.setStatusCode(String.valueOf(HttpStatus.BAD_REQUEST));
		return errorResponse;
	}
}
