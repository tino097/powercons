/**
 * 
 */
package com.sivakov.howerpause.controllers.exceptions;

/**
 * @author Tino097
 * 
 *         The {@link ReadingsErrorException} is thrown when one or more values
 *         set in the request are invalid.
 */
public class ReadingsErrorException extends RuntimeException {

	private static final long serialVersionUID = 1L;


	public ReadingsErrorException(String message) {
		super(message);
	}

	public ReadingsErrorException(String message, Throwable cause) {
		super(message, cause);
	}
}
