/**
 * 
 */
package com.sivakov.howerpause.controllers.exceptions;

/**
 * 
 * <p>
 * The {@link BadRequestException} is thrown when one or more values set in the
 * request are invalid.
 * 
 * @author Tino097
 * 
 */
public class BadRequestException extends RuntimeException {

	private static final long serialVersionUID = -5392480442042526056L;

	public BadRequestException(String message) {
		super(message);
	}

	public BadRequestException(String message, Throwable cause) {
		super(message, cause);

	}
}
