/**
 * 
 */
package com.sivakov.howerpause.models;

/**
 * @author tino097
 *
 */
public class Consumption extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Double consumption;
	private String connectionId;
	private Month month;

	public Consumption() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the consumption
	 */
	public Double getConsumption() {
		return consumption;
	}

	/**
	 * @param consumption
	 *            the consumption to set
	 */
	public void setConsumption(Double consumption) {
		this.consumption = consumption;
	}

	/**
	 * @return the connectionId
	 */
	public String getConnectionId() {
		return connectionId;
	}

	/**
	 * @param connectionId
	 *            the connectionId to set
	 */
	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	/**
	 * @return the month
	 */
	public Month getMonth() {
		return month;
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(Month month) {
		this.month = month;
	}

}
