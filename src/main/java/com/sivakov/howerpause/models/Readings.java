package com.sivakov.howerpause.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
public class Readings extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8184348048167950587L;

	@Column(nullable = false)
	private String connectionId;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private ProfileType profileType;

	@Column(nullable = false)
	@Enumerated(EnumType.STRING)
	private Month month;

	@Column(nullable = false)
	private Double meterReading;

	public Readings() {
		// TODO Auto-generated constructor stub
	}

	public String getConnectionId() {
		return connectionId;
	}

	public void setConnectionId(String connectionId) {
		this.connectionId = connectionId;
	}

	public ProfileType getProfileType() {
		return profileType;
	}

	public void setProfileType(ProfileType profileType) {
		this.profileType = profileType;
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(Month month) {
		this.month = month;
	}

	public Double getMeterReading() {
		return meterReading;
	}

	public void setMeterReading(Double meterReading) {
		this.meterReading = meterReading;
	}

	@Override
	public String toString() {
		return "Readings [connectionId=" + connectionId + ", profilType=" + profileType + ", month=" + month
				+ ", meterReading=" + meterReading + "]";
	}

}
