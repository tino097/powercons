/**
 * 
 */
package com.sivakov.howerpause.models;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * @author Tino097
 *
 */
@Entity
public class Profile extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2891381520952428660L;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Month month;

	@NotNull
	@Enumerated(EnumType.STRING)
	private ProfileType profileType;

	@NotNull
	private Double fraction;

	public Profile() {
		super();
	}

	public Profile(Month month, ProfileType profileType, Double fraction) {
		// TODO Auto-generated constructor stub
		this.month = month;
		this.profileType = profileType;
		this.fraction = fraction;
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(Month month) {
		this.month = month;
	}

	public ProfileType getProfileType() {
		return profileType;
	}

	public void setProfileType(ProfileType profileType) {
		this.profileType = profileType;
	}

	public Double getFraction() {
		return fraction;
	}

	public void setFraction(Double fraction) {
		this.fraction = fraction;
	}

	@Override
	public String toString() {
		return "Profile [ month=" + month + ", profileType=" + profileType + ", fraction=" + fraction + "]";
	}

}
