package com.sivakov.howerpause.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sivakov.howerpause.controllers.utils.Validator;
import com.sivakov.howerpause.models.Readings;
import com.sivakov.howerpause.repositories.ReadingsRepository;

@Service("readingsService")
public class ReadingsService implements IReadingsService {

	private static final Logger LOG = Logger.getLogger(ReadingsService.class);

	@Autowired
	ReadingsRepository readingsRepository;

	@Autowired
	Validator validator;

	public ReadingsService(ReadingsRepository readingsRepository) {
		this.readingsRepository = readingsRepository;
	}

	@Override
	public List<Readings> save(List<Readings> list) {
		LOG.info("Saving readings " + list);
		validator.validateMonthlyReadings(list);
		for (Readings readings : list) {
			Readings savedReding = readingsRepository.save(readings);
		}
		return list;
	}

	@Override
	public List<Readings> findByConnectionId(String connectionId) {
		// TODO Auto-generated method stub
		List<Readings> readings = readingsRepository.findByConnectionId(connectionId);
		return readings;
	}

	@Override
	public List<Readings> findByMonth(String month) {

		List<Readings> readings = readingsRepository.findByMonth(month);
		return readings;
	}

}
