/**
 * 
 */
package com.sivakov.howerpause.services;

import com.sivakov.howerpause.models.Consumption;

/**
 * @author tino097
 *
 */
public interface IConsumptionService {

	Consumption getConsumptionForConnectionId(String connectionId);

	Consumption getConsumptionForMonth(String month);
}
