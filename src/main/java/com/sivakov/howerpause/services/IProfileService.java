package com.sivakov.howerpause.services;

import java.util.List;

import com.sivakov.howerpause.models.Profile;

public interface IProfileService {

	Long save(Profile profile);

	List<Profile> getAll();

	List<Profile> findByProfileType(String profileType);

	Profile getOne(Long profileId);

	int deleteProfilesByType(String profileType);

}
