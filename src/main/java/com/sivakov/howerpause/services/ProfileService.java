/**
 * 
 */
package com.sivakov.howerpause.services;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sivakov.howerpause.models.Profile;
import com.sivakov.howerpause.repositories.ProfileRepository;

/**
 * @author Tino097
 *
 */
@Service("profileService")
public class ProfileService implements IProfileService {

	private static final Logger LOG = Logger.getLogger(ProfileService.class);

	@Autowired
	private ProfileRepository profileRepository;

	public ProfileService(ProfileRepository profileRepository) {
		this.profileRepository = profileRepository;
	}

	/*
	 * 
	 * 
	 * @param com.sivakov.howerpause.models.Profile
	 * 
	 * @return Long
	 */
	@Override
	@Transactional
	public Long save(Profile profile) {
		Profile savedProfile = profileRepository.save(profile);
		if (savedProfile != null && !savedProfile.equals(null))
			LOG.info("Succesfully saved: " + savedProfile);
		return savedProfile.getId();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Profile> getAll() {
		List<Profile> profiles = profileRepository.findAll();
		return profiles;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Profile> findByProfileType(String profileType) {
		List<Profile> profiles = profileRepository.findByProfileType(profileType);
		return profiles;
	}

	@Override
	@Transactional(readOnly = true)
	public Profile getOne(Long profileId) {
		Profile profile = profileRepository.findOne(profileId);
		return profile;
	}

	@Override
	@Transactional
	public int deleteProfilesByType(String profileType) {
		int result = profileRepository.deleteProfileByType(profileType);
		return result;
	}

}
