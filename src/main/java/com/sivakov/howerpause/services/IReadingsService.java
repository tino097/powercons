package com.sivakov.howerpause.services;

import java.util.List;

import com.sivakov.howerpause.models.Readings;

public interface IReadingsService {

	List<Readings> save(List<Readings> readings);

	List<Readings> findByConnectionId(String connectionId);

	List<Readings> findByMonth(String month);

}
