/**
 * 
 */
package com.sivakov.howerpause.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sivakov.howerpause.controllers.exceptions.CosumptionException;
import com.sivakov.howerpause.models.Consumption;
import com.sivakov.howerpause.models.Profile;
import com.sivakov.howerpause.models.ProfileType;
import com.sivakov.howerpause.models.Readings;

/**
 * @author tino097
 *
 */
@Service("consumptionService")
public class ConsumptionService implements IConsumptionService {

	@Autowired
	private ProfileService profileService;
	@Autowired
	private ReadingsService readingsService;

	@Override
	public Consumption getConsumptionForConnectionId(String connectionId) {

		Double sum = 0.0;
		Consumption consumption = new Consumption();
		List<Readings> readings = readingsService.findByConnectionId(connectionId);
		ProfileType profileType = readings.get(0).getProfileType();
		List<Profile> profiles = profileService.findByProfileType(profileType.name());
		if (profiles == null) {
			throw new CosumptionException("There is no profile for this conenction");
		}
		consumption.setConnectionId(connectionId);
		sum = readings.get(readings.size() - 1).getMeterReading() - readings.get(0).getMeterReading();
		consumption.setConsumption(sum);
		return consumption;
	}

	@Override
	public Consumption getConsumptionForMonth(String month) {
		Consumption consumption = new Consumption();
		Double sum = 0.0;
		List<Readings> readings = readingsService.findByMonth(month);
		for (Readings reading : readings) {
			List<Profile> profiles = profileService.findByProfileType(reading.getProfileType().name());
			for (Profile profile : profiles) {

			}
		}
		return consumption;
	}

}
