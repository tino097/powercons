/**
 * 
 */
package com.sivakov.howerpause.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sivakov.howerpause.models.Readings;

/**
 * @author Tino097
 *
 */
@Repository("readingsReposiotry")
public interface ReadingsRepository extends JpaRepository<Readings, Long> {

	@Query("select r from Readings r where r.connectionId=?1 order by r.month")
	List<Readings> findByConnectionId(String connectionId);

	@Query("select r from Readings r where r.month=?1 order by r.connectionId")
	List<Readings> findByMonth(String month);

}
