/**
 * 
 */
package com.sivakov.howerpause.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.sivakov.howerpause.models.Profile;
import com.sivakov.howerpause.services.ProfileService;

/**
 * The {@link} is custom JPA repository providing out of the box queries used by
 * {@link ProfileService}
 * 
 * @author Tino097
 *
 */
@Repository("profileRepository")
public interface ProfileRepository extends JpaRepository<Profile, Long> {

	// custom query using JPQL, best used when we need to create specific queries
	@Query("select p from Profile p where p.profileType = ?1 order by p.month ASC")
	List<Profile> findByProfileType(String profileType);

	// custom query using JPQL, deleting specific profile type
	@Query("delete from Profile p where p.profileType = ?1")
	@Modifying
	int deleteProfileByType(String profileType);
}
