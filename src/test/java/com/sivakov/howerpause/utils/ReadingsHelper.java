package com.sivakov.howerpause.utils;

public class ReadingsHelper {

	public static String json = "{\r\n" + 
			" \"readings\":[\r\n" + 
			" 	{\"connectionId\":\"0002\",\"month\":\"AUG\", \"profileType\":\"B\", \"meterReading\": 33},\r\n" + 
			"	\r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"SEP\", \"profileType\":\"B\", \"meterReading\": 45},\r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"OCT\", \"profileType\":\"B\", \"meterReading\": 55},\r\n" + 
			" {\"connectionId\":\"0001\", \"month\":\"JAN\", \"profileType\":\"A\", \"meterReading\": 2},\r\n" + 
			"\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"JUN\", \"profileType\":\"A\", \"meterReading\": 17},\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"JUL\", \"profileType\":\"A\", \"meterReading\": 27},\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"AUG\", \"profileType\":\"A\", \"meterReading\": 32.4},\r\n" + 
			"	\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"SEP\", \"profileType\":\"A\", \"meterReading\": 44},\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"OCT\", \"profileType\":\"A\", \"meterReading\": 49.1},\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"NOV\", \"profileType\":\"A\", \"meterReading\": 56},\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"DEC\", \"profileType\":\"A\", \"meterReading\": 65},\r\n" + 
			" \r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"MAY\", \"profileType\":\"B\", \"meterReading\": 21},\r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"JUN\", \"profileType\":\"B\", \"meterReading\": 25},\r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"JUL\", \"profileType\":\"B\", \"meterReading\": 31},\r\n" + 
			"\r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"NOV\", \"profileType\":\"B\", \"meterReading\": 76},\r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"DEC\", \"profileType\":\"B\", \"meterReading\": 89},\r\n" + 
			"	{\"connectionId\":\"0002\", \"month\":\"JAN\", \"profileType\":\"B\", \"meterReading\": 4},\r\n" + 
			"\r\n" + 
			" {\"connectionId\":\"0002\",\"month\":\"FEB\", \"profileType\":\"B\", \"meterReading\": 8},\r\n" + 
			" {\"connectionId\":\"0002\",\"month\":\"MAR\", \"profileType\":\"B\", \"meterReading\": 11},\r\n" + 
			"	{\"connectionId\":\"0002\",\"month\":\"APR\", \"profileType\":\"B\", \"meterReading\": 15},\r\n" + 
			"	 {\"connectionId\":\"0001\",\"month\":\"FEB\", \"profileType\":\"A\", \"meterReading\": 4},\r\n" + 
			" {\"connectionId\":\"0001\",\"month\":\"MAR\", \"profileType\":\"A\", \"meterReading\": 7.5},\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"APR\", \"profileType\":\"A\", \"meterReading\": 11},\r\n" + 
			"	{\"connectionId\":\"0001\",\"month\":\"MAY\", \"profileType\":\"A\", \"meterReading\": 21}\r\n" + 
			" ]\r\n" + 
			"}";
}
