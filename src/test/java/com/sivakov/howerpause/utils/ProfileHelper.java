package com.sivakov.howerpause.utils;

public class ProfileHelper {

	public static String json = "{\r\n" + 
			" \"profiles\":[\r\n" + 
			" {\"month\":\"JAN\", \"profileType\":\"B\", \"fraction\": 0.02},\r\n" + 
			" {\"month\":\"APR\", \"profileType\":\"B\", \"fraction\": 0.02},\r\n" + 
			" {\"month\":\"NOV\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"JAN\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"FEB\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"MAR\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"APR\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"FEB\", \"profileType\":\"B\", \"fraction\": 0.1},\r\n" + 
			"	\r\n" + 
			"	{\"month\":\"JUL\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"AUG\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"SEP\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"OCT\", \"profileType\":\"A\", \"fraction\": 0.1},\r\n" + 
			"	\r\n" + 
			"	{\"month\":\"DEC\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	\r\n" + 
			"	\r\n" + 
			"	{\"month\":\"MAR\", \"profileType\":\"B\", \"fraction\": 0.1},\r\n" + 
			"	\r\n" + 
			"	{\"month\":\"MAY\", \"profileType\":\"B\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"JUN\", \"profileType\":\"B\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"JUL\", \"profileType\":\"B\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"AUG\", \"profileType\":\"B\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"SEP\", \"profileType\":\"B\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"JUN\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"OCT\", \"profileType\":\"B\", \"fraction\": 0.2},\r\n" + 
			"	{\"month\":\"NOV\", \"profileType\":\"B\", \"fraction\": 0.2},\r\n" + 
			"	{\"month\":\"MAY\", \"profileType\":\"A\", \"fraction\": 0.02},\r\n" + 
			"	{\"month\":\"DEC\", \"profileType\":\"B\", \"fraction\": 0.2}\r\n" + 
			" ]\r\n" + 
			"}";
	
}
