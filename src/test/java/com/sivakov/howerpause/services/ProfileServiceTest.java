/**
 * 
 */
package com.sivakov.howerpause.services;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.sivakov.howerpause.AppTest;
import com.sivakov.howerpause.models.Month;
import com.sivakov.howerpause.models.Profile;
import com.sivakov.howerpause.models.ProfileType;
import com.sivakov.howerpause.repositories.ProfileRepository;
import com.sivakov.howerpause.services.ProfileService;

/**
 * @author Tino097
 *
 */
public class ProfileServiceTest extends AppTest {

	@Mock
	ProfileRepository profileRepository;

	@InjectMocks
	ProfileService profileService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() {

	}

	@Test
	public void testSaveProfile() {
		Profile profile = new Profile();
		profile.setId(4L);
		profile.setProfileType(ProfileType.A);
		profile.setMonth(Month.JUN);
		profile.setFraction(0.2);
		when(profileRepository.save(profile)).thenReturn(profile);
		Long p = profileService.save(profile);
		assertNotNull(p);

	}

}
