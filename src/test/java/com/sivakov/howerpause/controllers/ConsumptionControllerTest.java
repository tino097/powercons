package com.sivakov.howerpause.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.sivakov.howerpause.AppTest;
import com.sivakov.howerpause.repositories.ProfileRepository;
import com.sivakov.howerpause.repositories.ReadingsRepository;
import com.sivakov.howerpause.services.ConsumptionService;

@WebMvcTest(ConsumptionController.class)
public class ConsumptionControllerTest extends AppTest {

	@MockBean
	private ConsumptionService consumptionService;

	@Mock
	private ProfileRepository profileRepository;

	@Mock
	private ReadingsRepository readingsRepository;

	@Autowired
	private MockMvc mvc;

	@Before
	public void setup() throws IOException {
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testGetConsumptionFoId() throws Exception {
		this.mvc.perform(get("/api/consumptions?connectionId=0001")).andExpect(status().isOk()).andDo(print());
	}
}
