/**
 * 
 */
package com.sivakov.howerpause.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.sivakov.howerpause.AppTest;
import com.sivakov.howerpause.repositories.ProfileRepository;
import com.sivakov.howerpause.services.ProfileService;
import com.sivakov.howerpause.utils.ProfileHelper;

/**
 * @author Tino097
 *
 */
@WebMvcTest(ProfileController.class)
public class ProfileControllerTest extends AppTest {

	@MockBean
	private ProfileService profileService;

	@Mock
	private ProfileRepository profileRepository;

	@Autowired
	private MockMvc mvc;

	@Before
	public void setup() throws IOException {
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testSaveProfilesSendNoData() throws Exception {
		this.mvc.perform(post("/api/profiles").accept(MediaType.APPLICATION_JSON).content("")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isInternalServerError()).andDo(print());
	}

	@Test
	public void testSaveProfiles() throws Exception {

		this.mvc.perform(post("/api/profiles").accept(MediaType.APPLICATION_JSON).content(ProfileHelper.json)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated()).andDo(print());
	}

	@Test
	public void testGetProfileByType() throws Exception {
		this.mvc.perform(get("/api/profiles/profile/{profileType}", "A").accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk()).andDo(print());
	}
}
