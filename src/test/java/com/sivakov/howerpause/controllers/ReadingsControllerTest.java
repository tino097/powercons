/**
 * 
 */
package com.sivakov.howerpause.controllers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.sivakov.howerpause.AppTest;
import com.sivakov.howerpause.repositories.ReadingsRepository;
import com.sivakov.howerpause.services.ReadingsService;
import com.sivakov.howerpause.utils.ReadingsHelper;

/**
 * @author Tino097
 *
 */
@WebMvcTest(ReadingsController.class)
public class ReadingsControllerTest extends AppTest {

	@MockBean
	private ReadingsService readingsService;

	@Mock
	private ReadingsRepository readingsRepository;

	@Autowired
	private MockMvc mvc;

	@Before
	public void setup() throws IOException {
		MockitoAnnotations.initMocks(this);
	}

	@After
	public void tearDown() {
	}

	@Test
	public void testSaveReadings() throws Exception {
		this.mvc.perform(post("/api/readings").accept(MediaType.APPLICATION_JSON).content(ReadingsHelper.json)
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void testSaveReadingsNoData() throws Exception {
		this.mvc.perform(post("/api/readings").accept(MediaType.APPLICATION_JSON).content("")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(status().isInternalServerError()).andDo(print());
	}
}
