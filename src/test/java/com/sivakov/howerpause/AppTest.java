package com.sivakov.howerpause;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public abstract class AppTest {

	protected Logger LOG = LoggerFactory.getLogger(this.getClass());
}
